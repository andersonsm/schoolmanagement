package com.anderson.schoolmanagement.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A SchoolClass.
 */
@Entity
@Table(name = "school_class")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SchoolClass implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "start_date")
    private ZonedDateTime startDate;

    @Column(name = "end_date")
    private ZonedDateTime endDate;

    @NotNull
    @Size(min = 1)
    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "schoolClass", fetch = FetchType.EAGER)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Student> students = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "school_class_teachers",
               joinColumns = @JoinColumn(name="school_classes_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="teachers_id", referencedColumnName="id"))
    private Set<Teacher> teachers = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "school_class_subjects",
               joinColumns = @JoinColumn(name="school_classes_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="subjects_id", referencedColumnName="id"))
    private Set<Subject> subjects = new HashSet<>();

    @ManyToOne
    private School school;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public SchoolClass startDate(ZonedDateTime startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTime getEndDate() {
        return endDate;
    }

    public SchoolClass endDate(ZonedDateTime endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public SchoolClass name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public SchoolClass students(Set<Student> students) {
        this.students = students;
        return this;
    }

    public SchoolClass addStudents(Student student) {
        this.students.add(student);
        student.setSchoolClass(this);
        return this;
    }

    public SchoolClass removeStudents(Student student) {
        this.students.remove(student);
        student.setSchoolClass(null);
        return this;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public Set<Teacher> getTeachers() {
        return teachers;
    }

    public SchoolClass teachers(Set<Teacher> teachers) {
        this.teachers = teachers;
        return this;
    }

    public SchoolClass addTeachers(Teacher teacher) {
        this.teachers.add(teacher);
        teacher.getClasses().add(this);
        return this;
    }

    public SchoolClass removeTeachers(Teacher teacher) {
        this.teachers.remove(teacher);
        teacher.getClasses().remove(this);
        return this;
    }

    public void setTeachers(Set<Teacher> teachers) {
        this.teachers = teachers;
    }

    public Set<Subject> getSubjects() {
        return subjects;
    }

    public SchoolClass subjects(Set<Subject> subjects) {
        this.subjects = subjects;
        return this;
    }

    public SchoolClass addSubjects(Subject subject) {
        this.subjects.add(subject);
        return this;
    }

    public SchoolClass removeSubjects(Subject subject) {
        this.subjects.remove(subject);
        return this;
    }

    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }

    public School getSchool() {
        return school;
    }

    public SchoolClass school(School school) {
        this.school = school;
        return this;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SchoolClass schoolClass = (SchoolClass) o;
        if (schoolClass.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), schoolClass.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SchoolClass{" +
            "id=" + getId() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }
}
