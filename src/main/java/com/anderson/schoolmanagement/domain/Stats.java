package com.anderson.schoolmanagement.domain;

public class Stats {

    private int schoolsTotal;

    private int schoolClassesTotal;

    private int studentsTotal;

    public Stats(int schoolsTotal, int schoolClassesTotal, int studentsTotal) {
        this.schoolsTotal = schoolsTotal;
        this.schoolClassesTotal = schoolClassesTotal;
        this.studentsTotal = studentsTotal;
    }

    public int getSchoolsTotal() {
        return schoolsTotal;
    }

    public void setSchoolsTotal(int schoolsTotal) {
        this.schoolsTotal = schoolsTotal;
    }

    public int getSchoolClassesTotal() {
        return schoolClassesTotal;
    }

    public void setSchoolClassesTotal(int schoolClassesTotal) {
        this.schoolClassesTotal = schoolClassesTotal;
    }

    public int getStudentsTotal() {
        return studentsTotal;
    }

    public void setStudentsTotal(int studentsTotal) {
        this.studentsTotal = studentsTotal;
    }

    @Override
    public String toString() {
        return "Stats{" +
            "schoolsTotal=" + schoolsTotal +
            ", schoolClassesTotal=" + schoolClassesTotal +
            ", studentsTotal=" + studentsTotal +
            '}';
    }
}
