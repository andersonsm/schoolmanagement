package com.anderson.schoolmanagement.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Teacher.
 */
@Entity
@Table(name = "teacher")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Teacher implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @ManyToMany(mappedBy = "teachers")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<School> schools = new HashSet<>();

    @ManyToMany(mappedBy = "teachers")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SchoolClass> classes = new HashSet<>();

    @ManyToMany(mappedBy = "teachers")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Subject> subjects = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Teacher name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<School> getSchools() {
        return schools;
    }

    public Teacher schools(Set<School> schools) {
        this.schools = schools;
        return this;
    }

    public Teacher addSchools(School school) {
        this.schools.add(school);
        school.getTeachers().add(this);
        return this;
    }

    public Teacher removeSchools(School school) {
        this.schools.remove(school);
        school.getTeachers().remove(this);
        return this;
    }

    public void setSchools(Set<School> schools) {
        this.schools = schools;
    }

    public Set<SchoolClass> getClasses() {
        return classes;
    }

    public Teacher classes(Set<SchoolClass> schoolClasses) {
        this.classes = schoolClasses;
        return this;
    }

    public Teacher addClasses(SchoolClass schoolClass) {
        this.classes.add(schoolClass);
        schoolClass.getTeachers().add(this);
        return this;
    }

    public Teacher removeClasses(SchoolClass schoolClass) {
        this.classes.remove(schoolClass);
        schoolClass.getTeachers().remove(this);
        return this;
    }

    public void setClasses(Set<SchoolClass> schoolClasses) {
        this.classes = schoolClasses;
    }

    public Set<Subject> getSubjects() {
        return subjects;
    }

    public Teacher subjects(Set<Subject> subjects) {
        this.subjects = subjects;
        return this;
    }

    public Teacher addSubjects(Subject subject) {
        this.subjects.add(subject);
        subject.getTeachers().add(this);
        return this;
    }

    public Teacher removeSubjects(Subject subject) {
        this.subjects.remove(subject);
        subject.getTeachers().remove(this);
        return this;
    }

    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Teacher teacher = (Teacher) o;
        if (teacher.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), teacher.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Teacher{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
