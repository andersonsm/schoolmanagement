package com.anderson.schoolmanagement.repository;

import com.anderson.schoolmanagement.domain.SchoolClass;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the SchoolClass entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SchoolClassRepository extends JpaRepository<SchoolClass,Long> {

    @Query("select distinct school_class from SchoolClass school_class left join fetch school_class.teachers left join fetch school_class.subjects")
    List<SchoolClass> findAllWithEagerRelationships();

    @Query("select school_class from SchoolClass school_class left join fetch school_class.teachers left join fetch school_class.subjects where school_class.id =:id")
    SchoolClass findOneWithEagerRelationships(@Param("id") Long id);

}
