package com.anderson.schoolmanagement.repository;

import com.anderson.schoolmanagement.domain.School;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the School entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SchoolRepository extends JpaRepository<School,Long> {

    @Query("select distinct school from School school left join fetch school.teachers")
    List<School> findAllWithEagerRelationships();

    @Query("select school from School school left join fetch school.teachers where school.id =:id")
    School findOneWithEagerRelationships(@Param("id") Long id);

}
