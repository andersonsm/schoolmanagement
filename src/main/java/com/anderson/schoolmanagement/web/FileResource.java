package com.anderson.schoolmanagement.web;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;

@Controller
public class FileResource {

    private final String UPLOADED_FOLDER = System.getProperty("user.dir") + "\\target\\uploads\\";

    public FileResource() {}

    @GetMapping("/schools/file/{id}")
    public void getFile(@PathVariable Long id, HttpServletResponse response) {
        File file = new File(UPLOADED_FOLDER + id + ".pdf");
        if(file.exists()){
            try {
                FileInputStream stream = new FileInputStream(file);
                response.setContentType("application/pdf");
                response.setHeader("Content-disposition", "attachment; filename=" + file.getName());
                IOUtils.copy(stream,response.getOutputStream());
                stream.close();
            }
            catch (Exception e) {
                response.setStatus(500);
            }
        } else {
            response.setStatus(404);
        }
    }
}
