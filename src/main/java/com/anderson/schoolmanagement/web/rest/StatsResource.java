package com.anderson.schoolmanagement.web.rest;

import com.anderson.schoolmanagement.domain.Stats;
import com.anderson.schoolmanagement.repository.SchoolClassRepository;
import com.anderson.schoolmanagement.repository.SchoolRepository;
import com.anderson.schoolmanagement.repository.StudentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class StatsResource {

    private final Logger log = LoggerFactory.getLogger(StatsResource.class);

    private final SchoolRepository schoolRepository;

    private final SchoolClassRepository schoolClassRepository;

    private final StudentRepository studentRepository;

    public StatsResource(SchoolRepository schoolRepository, SchoolClassRepository schoolClassRepository, StudentRepository studentRepository){
        this.schoolRepository = schoolRepository;
        this.schoolClassRepository = schoolClassRepository;
        this.studentRepository = studentRepository;
    }

    @GetMapping("/stats")
    public Stats getStats(){
        Stats stats = new Stats(schoolRepository.findAll().size(), schoolClassRepository.findAll().size(), studentRepository.findAll().size());
        return stats;
    }
}
