/**
 * View Models used by Spring MVC REST controllers.
 */
package com.anderson.schoolmanagement.web.rest.vm;
