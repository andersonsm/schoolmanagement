import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ng2-webstorage';

import { SchoolManagementSharedModule, UserRouteAccessService } from './shared';
import { SchoolManagementHomeModule } from './home/home.module';
import { SchoolManagementAdminModule } from './admin/admin.module';
import { SchoolManagementAccountModule } from './account/account.module';
import { SchoolManagementEntityModule } from './entities/entity.module';

import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';

import {UtilityService} from './shared/utility.service';

import {
    JhiMainComponent,
    LayoutRoutingModule,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ErrorComponent
} from './layouts';

@NgModule({
    imports: [
        BrowserModule,
        LayoutRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        SchoolManagementSharedModule,
        SchoolManagementHomeModule,
        SchoolManagementAdminModule,
        SchoolManagementAccountModule,
        SchoolManagementEntityModule
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService,
        UtilityService
    ],
    bootstrap: [ JhiMainComponent ]
})
export class SchoolManagementAppModule {}
