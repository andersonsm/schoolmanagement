import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SchoolManagementSchoolModule } from './school/school.module';
import { SchoolManagementSchoolClassModule } from './school-class/school-class.module';
import { SchoolManagementSubjectModule } from './subject/subject.module';
import { SchoolManagementTeacherModule } from './teacher/teacher.module';
import { SchoolManagementStudentModule } from './student/student.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        SchoolManagementSchoolModule,
        SchoolManagementSchoolClassModule,
        SchoolManagementSubjectModule,
        SchoolManagementTeacherModule,
        SchoolManagementStudentModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SchoolManagementEntityModule {}
