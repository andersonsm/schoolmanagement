export * from './school-class.model';
export * from './school-class-popup.service';
export * from './school-class.service';
export * from './school-class-dialog.component';
export * from './school-class-delete-dialog.component';
export * from './school-class-detail.component';
export * from './school-class.component';
export * from './school-class.route';
