import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService } from 'ng-jhipster';

import { SchoolClass } from './school-class.model';
import { SchoolClassPopupService } from './school-class-popup.service';
import { SchoolClassService } from './school-class.service';
import { Teacher, TeacherService } from '../teacher';
import { Subject, SubjectService } from '../subject';
import { School, SchoolService } from '../school';
import { ResponseWrapper } from '../../shared';
import { UtilityService} from '../../shared/utility.service';

@Component({
    selector: 'jhi-school-class-dialog',
    templateUrl: './school-class-dialog.component.html'
})
export class SchoolClassDialogComponent implements OnInit {

    schoolClass: SchoolClass;
    authorities: any[];
    isSaving: boolean;

    teachers: Teacher[];

    subjects: Subject[];

    schools: School[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private schoolClassService: SchoolClassService,
        private teacherService: TeacherService,
        private subjectService: SubjectService,
        private schoolService: SchoolService,
        private eventManager: EventManager,
        public utilityService: UtilityService
    ) {
    }

    ngOnInit() {
        if (this.schoolClass.id === undefined) {
            this.schoolClass.teachers = [];
            this.schoolClass.subjects = [];
        }
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.teacherService.query()
            .subscribe((res: ResponseWrapper) => { this.teachers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.subjectService.query()
            .subscribe((res: ResponseWrapper) => { this.subjects = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.schoolService.query()
            .subscribe((res: ResponseWrapper) => { this.schools = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.schoolClass.id !== undefined) {
            this.subscribeToSaveResponse(
                this.schoolClassService.update(this.schoolClass), false);
        } else {
            this.subscribeToSaveResponse(
                this.schoolClassService.create(this.schoolClass), true);
        }
    }

    private subscribeToSaveResponse(result: Observable<SchoolClass>, isCreated: boolean) {
        result.subscribe((res: SchoolClass) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: SchoolClass, isCreated: boolean) {
        this.alertService.success(
            isCreated ? `A new School Class is created with identifier ${result.id}`
            : `A School Class is updated with identifier ${result.id}`,
            null, null);

        this.eventManager.broadcast({ name: 'schoolClassListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackTeacherById(index: number, item: Teacher) {
        return item.id;
    }

    trackSubjectById(index: number, item: Subject) {
        return item.id;
    }

    trackSchoolById(index: number, item: School) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-school-class-popup',
    template: ''
})
export class SchoolClassPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private schoolClassPopupService: SchoolClassPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.schoolClassPopupService
                    .open(SchoolClassDialogComponent, params['id']);
            } else {
                this.modalRef = this.schoolClassPopupService
                    .open(SchoolClassDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
