import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { SchoolClass } from './school-class.model';
import { SchoolClassService } from './school-class.service';
@Injectable()
export class SchoolClassPopupService {
    private isOpen = false;
    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private schoolClassService: SchoolClassService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.schoolClassService.find(id).subscribe((schoolClass) => {
                schoolClass.startDate = this.datePipe
                    .transform(schoolClass.startDate, 'yyyy-MM-ddThh:mm');
                schoolClass.endDate = this.datePipe
                    .transform(schoolClass.endDate, 'yyyy-MM-ddThh:mm');
                this.schoolClassModalRef(component, schoolClass);
            });
        } else {
            return this.schoolClassModalRef(component, new SchoolClass());
        }
    }

    schoolClassModalRef(component: Component, schoolClass: SchoolClass): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.schoolClass = schoolClass;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
