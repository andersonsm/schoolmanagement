import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager, ParseLinks, PaginationUtil, AlertService } from 'ng-jhipster';

import { SchoolClass } from './school-class.model';
import { SchoolClassService } from './school-class.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-school-class',
    templateUrl: './school-class.component.html'
})
export class SchoolClassComponent implements OnInit, OnDestroy {
schoolClasses: SchoolClass[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private schoolClassService: SchoolClassService,
        private alertService: AlertService,
        private eventManager: EventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.schoolClassService.query().subscribe(
            (res: ResponseWrapper) => {
                this.schoolClasses = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSchoolClasses();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SchoolClass) {
        return item.id;
    }
    registerChangeInSchoolClasses() {
        this.eventSubscriber = this.eventManager.subscribe('schoolClassListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
