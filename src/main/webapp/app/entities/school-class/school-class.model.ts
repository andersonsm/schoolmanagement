import { Student } from '../student';
import { Teacher } from '../teacher';
import { Subject } from '../subject';
import { School } from '../school';
export class SchoolClass {
    constructor(
        public id?: number,
        public startDate?: any,
        public endDate?: any,
        public name?: string,
        public students?: Student,
        public teachers?: Teacher,
        public subjects?: Subject,
        public school?: School,
    ) {
    }
}
