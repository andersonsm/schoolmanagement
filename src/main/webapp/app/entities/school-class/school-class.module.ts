import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SchoolManagementSharedModule } from '../../shared';
import {
    SchoolClassService,
    SchoolClassPopupService,
    SchoolClassComponent,
    SchoolClassDetailComponent,
    SchoolClassDialogComponent,
    SchoolClassPopupComponent,
    SchoolClassDeletePopupComponent,
    SchoolClassDeleteDialogComponent,
    schoolClassRoute,
    schoolClassPopupRoute,
} from './';

const ENTITY_STATES = [
    ...schoolClassRoute,
    ...schoolClassPopupRoute,
];

@NgModule({
    imports: [
        SchoolManagementSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        SchoolClassComponent,
        SchoolClassDetailComponent,
        SchoolClassDialogComponent,
        SchoolClassDeleteDialogComponent,
        SchoolClassPopupComponent,
        SchoolClassDeletePopupComponent,
    ],
    entryComponents: [
        SchoolClassComponent,
        SchoolClassDialogComponent,
        SchoolClassPopupComponent,
        SchoolClassDeleteDialogComponent,
        SchoolClassDeletePopupComponent,
    ],
    providers: [
        SchoolClassService,
        SchoolClassPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SchoolManagementSchoolClassModule {}
