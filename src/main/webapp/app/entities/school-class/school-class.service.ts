import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { DateUtils } from 'ng-jhipster';

import { SchoolClass } from './school-class.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class SchoolClassService {

    private resourceUrl = 'api/school-classes';

    constructor(private http: Http, private dateUtils: DateUtils) { }

    create(schoolClass: SchoolClass): Observable<SchoolClass> {
        const copy = this.convert(schoolClass);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(schoolClass: SchoolClass): Observable<SchoolClass> {
        const copy = this.convert(schoolClass);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<SchoolClass> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.startDate = this.dateUtils
            .convertDateTimeFromServer(entity.startDate);
        entity.endDate = this.dateUtils
            .convertDateTimeFromServer(entity.endDate);
    }

    private convert(schoolClass: SchoolClass): SchoolClass {
        const copy: SchoolClass = Object.assign({}, schoolClass);

        copy.startDate = this.dateUtils.toDate(schoolClass.startDate);

        copy.endDate = this.dateUtils.toDate(schoolClass.endDate);
        return copy;
    }
}
