import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager  } from 'ng-jhipster';

import { School } from './school.model';
import { SchoolService } from './school.service';
import {Observable} from 'rxjs/Observable';
import {Http} from '@angular/http';

@Component({
    selector: 'jhi-school-detail',
    templateUrl: './school-detail.component.html'
})
export class SchoolDetailComponent implements OnInit, OnDestroy {

    school: School;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    public uploadError: boolean;

    constructor(
        private eventManager: EventManager,
        private schoolService: SchoolService,
        private route: ActivatedRoute,
        private http: Http
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSchools();
    }

    load(id) {
        this.schoolService.find(id).subscribe((school) => {
            this.school = school;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSchools() {
        this.eventSubscriber = this.eventManager.subscribe(
            'schoolListModification',
            (response) => this.load(this.school.id)
        );
    }

    onUploadSuccess() {
        this.uploadError = false;
        this.load(this.school.id);
    }

    onUploadFail() {
        this.uploadError = true;
    }

    fileChange(event) {
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            const formData: FormData = new FormData();
            formData.append('file', file, file.name);
            const headers = new Headers();
            headers.append('Content-Type', 'multipart/form-data');
            headers.append('Accept', 'application/json');
            const url = this.schoolService.getResourceUrl() + '/file/' + this.school.id;
            this.http.post(`${url}`, formData)
                .map((res) => res.json())
                .catch((error) => Observable.throw(error))
                .subscribe(
                    (data) => this.onUploadSuccess(),
                    (error) => this.onUploadFail()
                );
        }
    }
}
