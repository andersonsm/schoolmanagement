import { User } from '../../shared';
import { SchoolClass } from '../school-class';
import { Teacher } from '../teacher';
export class School {
    constructor(
        public id?: number,
        public name?: string,
        public address?: string,
        public city?: string,
        public state?: string,
        public country?: string,
        public phone?: string,
        public docAddress?: string,
        public principal?: User,
        public classes?: SchoolClass,
        public teachers?: Teacher,
    ) {
    }
}
