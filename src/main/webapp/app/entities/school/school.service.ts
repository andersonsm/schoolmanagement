import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { School } from './school.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class SchoolService {

    private resourceUrl = 'api/schools';

    constructor(private http: Http) { }

    create(school: School): Observable<School> {
        const copy = this.convert(school);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(school: School): Observable<School> {
        const copy = this.convert(school);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<School> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(school: School): School {
        const copy: School = Object.assign({}, school);
        return copy;
    }

    public getResourceUrl(): string {
        return this.resourceUrl;
    }
}
