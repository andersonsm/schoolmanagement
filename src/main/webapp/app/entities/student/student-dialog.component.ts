import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService } from 'ng-jhipster';

import { Student } from './student.model';
import { StudentPopupService } from './student-popup.service';
import { StudentService } from './student.service';
import { School, SchoolService } from '../school';
import { SchoolClass, SchoolClassService } from '../school-class';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-student-dialog',
    templateUrl: './student-dialog.component.html'
})
export class StudentDialogComponent implements OnInit {

    student: Student;
    authorities: any[];
    isSaving: boolean;

    schools: School[];

    schoolClasses: SchoolClass[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private studentService: StudentService,
        private schoolService: SchoolService,
        private schoolClassService: SchoolClassService,
        private eventManager: EventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.schoolService
            .query({filter: 'student-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.student.school || !this.student.school.id) {
                    this.schools = res.json;
                } else {
                    this.schoolService
                        .find(this.student.school.id)
                        .subscribe((subRes: School) => {
                            this.schools = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.schoolClassService.query()
            .subscribe((res: ResponseWrapper) => { this.schoolClasses = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        this.student.school = this.student.schoolClass.school;
        if (this.student.id !== undefined) {
            this.subscribeToSaveResponse(
                this.studentService.update(this.student), false);
        } else {
            this.subscribeToSaveResponse(
                this.studentService.create(this.student), true);
        }
    }

    private subscribeToSaveResponse(result: Observable<Student>, isCreated: boolean) {
        result.subscribe((res: Student) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Student, isCreated: boolean) {
        this.alertService.success(
            isCreated ? `A new Student is created with identifier ${result.id}`
            : `A Student is updated with identifier ${result.id}`,
            null, null);

        this.eventManager.broadcast({ name: 'studentListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackSchoolById(index: number, item: School) {
        return item.id;
    }

    trackSchoolClassById(index: number, item: SchoolClass) {
        return item.id;
    }

    /*updateClassesBySchool(school) {
        this.student.school = school.id;
        this.schoolClassesBySchool = [];
        if (!school) {
            return;
        }
        for (let i = 0; i < this.schoolClasses.length; i++) {
            if (this.schoolClasses[i].school.id === school.id) {
                this.schoolClassesBySchool.push(this.schoolClasses[i]);
            }
        }
    }*/
}

@Component({
    selector: 'jhi-student-popup',
    template: ''
})
export class StudentPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private studentPopupService: StudentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.studentPopupService
                    .open(StudentDialogComponent, params['id']);
            } else {
                this.modalRef = this.studentPopupService
                    .open(StudentDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
