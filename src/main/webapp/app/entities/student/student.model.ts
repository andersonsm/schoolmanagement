import { School } from '../school';
import { SchoolClass } from '../school-class';
export class Student {
    constructor(
        public id?: number,
        public name?: string,
        public age?: number,
        public email?: string,
        public address?: string,
        public city?: string,
        public state?: string,
        public country?: string,
        public phone?: string,
        public school?: School,
        public schoolClass?: SchoolClass,
    ) {
    }
}
