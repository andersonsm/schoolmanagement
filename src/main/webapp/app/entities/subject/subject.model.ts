import { Teacher } from '../teacher';
export class Subject {
    constructor(
        public id?: number,
        public name?: string,
        public teachers?: Teacher,
    ) {
    }
}
