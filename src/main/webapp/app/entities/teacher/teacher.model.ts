import { School } from '../school';
import { SchoolClass } from '../school-class';
import { Subject } from '../subject';
export class Teacher {
    constructor(
        public id?: number,
        public name?: string,
        public schools?: School,
        public classes?: SchoolClass,
        public subjects?: Subject,
    ) {
    }
}
