import {Injectable} from '@angular/core';

@Injectable()
export class UtilityService {
    checkElementInArrayById(a: Array<any>, id) {
        for (let i = 0; i < a.length; i++) {
            if (a[i].id === id) {
                return true;
            }
        }

        return false;
    }

    toggleElementInArray(a: Array<any>, e: any) {
        for (let i = 0; i < a.length; i++) {
            if (a[i].id === e.id) {
                a.splice(i, 1);
                return;
            }
        }
        a.push(e);
    }
}
