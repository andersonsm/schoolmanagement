import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils, EventManager } from 'ng-jhipster';
import { SchoolManagementTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { SchoolClassDetailComponent } from '../../../../../../main/webapp/app/entities/school-class/school-class-detail.component';
import { SchoolClassService } from '../../../../../../main/webapp/app/entities/school-class/school-class.service';
import { SchoolClass } from '../../../../../../main/webapp/app/entities/school-class/school-class.model';

describe('Component Tests', () => {

    describe('SchoolClass Management Detail Component', () => {
        let comp: SchoolClassDetailComponent;
        let fixture: ComponentFixture<SchoolClassDetailComponent>;
        let service: SchoolClassService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SchoolManagementTestModule],
                declarations: [SchoolClassDetailComponent],
                providers: [
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    SchoolClassService,
                    EventManager
                ]
            }).overrideTemplate(SchoolClassDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SchoolClassDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SchoolClassService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new SchoolClass(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.schoolClass).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
